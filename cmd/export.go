package cmd

import (
	"errors"
	"fmt"
	"github.com/urfave/cli/v2"
	"gitlab.com/yduperis/secrets-manager/lib"
	"gitlab.com/yduperis/secrets-manager/lib/connector"
	"golang.org/x/crypto/ssh/terminal"
	"io/ioutil"
	"log"
	"syscall"
)

const (
	fei   string = "input-connector"
	feo   string = "output-connector"
	feki  string = "keepass-input-file"
	fekp  string = "keepass-password"
	fekpf string = "keepass-password-file"
	feies string = "ignore-existing-secrets"
	feoes string = "overwrite-existing-secrets"
	feg   string = "group"
)

var (
	availableICTs []string = []string{"keepass"}
	availableOCTs []string = []string{"docker"}
)

func CliExportCmd() *cli.Command {
	return &cli.Command{
		Name:    "export",
		Aliases: []string{"e"},
		Usage:   "Pipe secrets from an input source to an output target by using specific connectors",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     fei,
				Aliases:  []string{"i"},
				Usage:    "Type of input connector (available: keepass)",
				Required: true,
			},
			&cli.StringFlag{
				Name:     feo,
				Aliases:  []string{"o"},
				Usage:    "Type of output connector (available: docker)",
				Required: true,
			},
			&cli.StringFlag{
				Name:     feki,
				Aliases:  []string{"ki"},
				Usage:    "(Keepass Input Connector) KDBX file path",
				Required: false,
			},
			&cli.StringFlag{
				Name:     fekp,
				Aliases:  []string{"kp"},
				Usage:    "(Keepass Input Connector) [INSECURE] Password of KDBX file (will be prompted if not provided)",
				Required: false,
			},
			&cli.StringFlag{
				Name:     fekpf,
				Aliases:  []string{"kpf"},
				Usage:    "(Keepass Input Connector) Path of a file containing the password of KDBX file (will be prompted if not provided)",
				Required: false,
			},
			&cli.BoolFlag{
				Name:    feies,
				Aliases: []string{"k"},
				Usage:   "Ignores secrets that already exists in the output target",
			},
			&cli.BoolFlag{
				Name:    feoes,
				Aliases: []string{"f"},
				Usage:   "Overwrite secrets that already exists in the output target",
			},
			&cli.StringFlag{
				Name:    feg,
				Aliases: []string{"g"},
				Usage:   fmt.Sprintf("Group of secrets to import, all but in groups '%s' will be imported", connector.IgnoredGroupName),
			},
		},
		Action: exportAction,
	}
}

func exportAction(c *cli.Context) error {
	ict := c.String(fei)
	oct := c.String(feo)
	fmt.Printf("Input connector: %s\n", ict)
	fmt.Printf("Output connector: %s\n", oct)

	if !lib.SliceContains(availableICTs, ict) {
		return errors.New(fmt.Sprintf("unsupported input connector: %s", ict))
	}

	if !lib.SliceContains(availableOCTs, oct) {
		return errors.New(fmt.Sprintf("unsupported output connector: %s", oct))
	}

	var ic lib.InputConnector
	if ict == "keepass" {
		ki := c.String(feki)
		if ki == "" {
			return errors.New(fmt.Sprintf("cannot use Keepass Input Connector without providing path "+
				"to KDBX with '--%s' option", feki))
		}

		kp := c.String(fekp)
		kpf := c.String(fekpf)
		if kp != "" && kpf != "" {
			return errors.New("cannot provide a password for the KDBX file and a password file")
		}
		var password string
		if kp != "" {
			password = kp
		} else if kpf != "" {
			b, err := ioutil.ReadFile(kpf)
			if err != nil {
				return err
			}
			password = string(b)
		} else {
			fmt.Printf("Enter password to '%s':\t\n", ki)
			b, err := terminal.ReadPassword(syscall.Stdin)
			if err != nil {
				return err
			}
			password = string(b)
		}
		ric, err := connector.NewKeypassInputConnector(ki, password)
		ic = ric
		if err != nil {
			return err
		}
	}

	var oc lib.OutputConnector
	if oct == "docker" {
		roc, err := connector.NewDockerOutputConnector()
		oc = roc
		if err != nil {
			return err
		}
	}

	g := c.String(feg)
	var readArgs *lib.ReadSecretsArgs
	if g == "" {
		readArgs = nil
	} else {
		readArgs = &lib.ReadSecretsArgs{Group: g}
	}

	if c.Bool(feies) && c.Bool(feoes) {
		return errors.New("cannot overwrite and ignore existing secrets")
	}
	var eshb lib.ExistingSecretHandlingBehavior
	if c.Bool(feies) {
		eshb = lib.IgnoreExistingSecret
	} else if c.Bool(feoes) {
		eshb = lib.OverwriteExistingSecret
	} else {
		eshb = lib.ErrorOnExistingSecret
	}

	writeArgs := lib.WriteSecretsArgs{ExistingSecretHandlingBehavior: eshb}
	ignoredExistingSecrets, err := lib.ImportSecrets(ic, oc, readArgs, &writeArgs)
	if err != nil {
		return err
	}
	log.Printf("Secrets successfully exported ! %d secrets ignored", len(ignoredExistingSecrets))
	return nil
}
