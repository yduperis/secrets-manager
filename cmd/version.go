package cmd

import (
	"fmt"
	"github.com/urfave/cli/v2"
)

const (
	CLIVersion string = "v1.0.1"
)

func CliVersionCmd() *cli.Command {
	return &cli.Command{
		Name:    "version",
		Aliases: []string{"v"},
		Usage:   "Print the version",
		Action: func(context *cli.Context) error {
			fmt.Printf("secrets-manager version %s\n", CLIVersion)
			return nil
		},
	}
}
