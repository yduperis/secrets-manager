package connector

import (
	"context"
	"errors"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/swarm"
	"gitlab.com/yduperis/secrets-manager/lib"
	"log"

	"github.com/docker/docker/client"
)

type DockerOutputConnector struct {
	client  *client.Client
	context context.Context
}

func NewDockerOutputConnector() (oc *DockerOutputConnector, err error) {
	ctx := context.Background()
	cli, err := client.NewEnvClient()
	//cmd, err := client.NewClientWithOpts(client.FromEnv, client.WithVersion("1.40"))
	if err != nil {
		return nil, err
	}

	log.Println("Testing connection to Docker daemon")
	version, err := cli.ServerVersion(ctx)
	if err != nil {
		return nil, err
	}

	_, err = cli.SwarmInspect(ctx)
	if err != nil {
		return nil, errors.New("no Docker Swarm is configured on the local node, " +
			"secrets can only be managed within a swarm")
	}

	log.Println(fmt.Sprintf("Docker Output connector established connexion with local Docker daemon (version %s)", version.APIVersion))

	return &DockerOutputConnector{
		client:  cli,
		context: ctx,
	}, nil
}

func (oc DockerOutputConnector) WriteSecrets(secrets []lib.ManagedSecret, writeArgs *lib.WriteSecretsArgs) ([]lib.ManagedSecret, error) {
	log.Println(fmt.Sprintf("Importing %d secrets", len(secrets)))
	ignoredExistingSecrets := emptySecrets()

	if writeArgs == nil {
		writeArgs = &lib.WriteSecretsArgs{ExistingSecretHandlingBehavior: lib.ErrorOnExistingSecret}
	}

	args := filters.NewArgs()
	es, err := oc.client.SecretList(oc.context, types.SecretListOptions{Filters: args})
	if err != nil {
		return ignoredExistingSecrets, err
	}

	esNames := make(map[string]string, len(es))
	for _, s := range es {
		esNames[s.Spec.Name] = s.ID
	}

	for _, s := range secrets {
		if lib.MapContainsKey(esNames, s.Name) && writeArgs.ExistingSecretHandlingBehavior == lib.ErrorOnExistingSecret {
			return ignoredExistingSecrets, errors.New("some secrets are already declared, use overwrite/ignore options to avoid this error")
		}
	}

	for _, s := range secrets {
		if lib.MapContainsKey(esNames, s.Name) {
			if writeArgs.ExistingSecretHandlingBehavior == lib.OverwriteExistingSecret {
				err = oc.client.SecretRemove(oc.context, esNames[s.Name])
				if err != nil {
					return ignoredExistingSecrets, err
				}
				_, err = oc.client.SecretCreate(
					oc.context,
					swarm.SecretSpec{Annotations: swarm.Annotations{Name: s.Name}, Data: s.Value})
			} else {
				ignoredExistingSecrets = append(ignoredExistingSecrets, s)
				log.Println(fmt.Sprintf("Ignored existing secret '%s', use overwrite option to overwrite it", s.Name))
			}
		} else {
			_, err = oc.client.SecretCreate(
				oc.context,
				swarm.SecretSpec{Annotations: swarm.Annotations{Name: s.Name}, Data: s.Value})
		}
		if err != nil {
			return ignoredExistingSecrets, err
		}
	}

	return ignoredExistingSecrets, nil
}

func (oc DockerOutputConnector) GetName() string {
	return "Docker client"
}

func (oc DockerOutputConnector) Close() error {
	return oc.client.Close()
}
