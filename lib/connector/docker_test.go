package connector

import (
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"gitlab.com/yduperis/secrets-manager/lib"
	"math/rand"
	"testing"
	"time"
)

func TestDockerOutputConnector_GetName(t *testing.T) {
	oc, err := NewDockerOutputConnector()
	if err != nil {
		t.Error(err)
	}
	defer oc.Close()

	if oc.GetName() != "Docker client" {
		t.Error("Wrong name for output connector, expected 'Docker client'")
	}
}

func TestDockerOutputConnector_WriteSecrets(t *testing.T) {
	oc, err := NewDockerOutputConnector()
	if err != nil {
		t.Error(err)
	}
	defer oc.Close()

	nbSecrets := 2
	secrets := make([]lib.ManagedSecret, nbSecrets)
	for i := 0; i < nbSecrets; i++ {
		secrets[i] = lib.ManagedSecret{
			Name:  randomString(6),
			Value: []byte(randomString(10)),
		}
	}

	ignoredExistingSecrets, err := oc.WriteSecrets(secrets, nil)
	if err != nil {
		t.Error(err)
	}

	ss, err := oc.client.SecretList(oc.context, types.SecretListOptions{Filters: filters.NewArgs()})
	ssn := make(map[string]string, len(ss))
	for _, s := range ss {
		ssn[s.Spec.Name] = s.ID
	}

	defer func() {
		for _, s := range secrets {
			oc.client.SecretRemove(oc.context, ssn[s.Name])
		}
	}()

	if len(ignoredExistingSecrets) != 0 {
		t.Error("No existing secrets must be ignored")
	}

	for _, s := range secrets {
		if !lib.MapContainsKey(ssn, s.Name) {
			t.Error(fmt.Sprintf("Secret '%s' has not been declared", s.Name))
		}
	}

	_, err = oc.WriteSecrets(secrets, nil)
	if err == nil {
		t.Error("Trying to redeclare secrets without overwrite option should trigger an error")
	}

	ignoredExistingSecrets, err = oc.WriteSecrets(secrets, &lib.WriteSecretsArgs{ExistingSecretHandlingBehavior: lib.OverwriteExistingSecret})
	if len(ignoredExistingSecrets) != 0 {
		t.Error("No existing secrets must be ignored")
	}
	if err != nil {
		t.Error(err)
	}
	ss, err = oc.client.SecretList(oc.context, types.SecretListOptions{Filters: filters.NewArgs()})
	if err != nil {
		t.Error(err)
	}
	for _, s := range ss {
		ssn[s.Spec.Name] = s.ID
	}

	for _, s := range secrets {
		if !lib.MapContainsKey(ssn, s.Name) {
			t.Error(fmt.Sprintf("Secret '%s' has not been declared", s.Name))
		}
	}

	ignoredExistingSecrets, err = oc.WriteSecrets(secrets, &lib.WriteSecretsArgs{ExistingSecretHandlingBehavior: lib.IgnoreExistingSecret})
	if err != nil {
		t.Error(err)
	}
	if len(ignoredExistingSecrets) != nbSecrets {
		t.Error("Must have ignored all existing secrets")
	}
}

var seededRand = rand.New(rand.NewSource(time.Now().UnixNano()))

func randomString(length int) string {
	charset := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-"
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}
