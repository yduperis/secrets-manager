package connector

import (
	"errors"
	"fmt"
	"github.com/tobischo/gokeepasslib/v3"
	"gitlab.com/yduperis/secrets-manager/lib"
	"os"
	"path/filepath"
)

const (
	IgnoredGroupName string = ".ignore"
)

type KeepassInputConnector struct {
	db   *gokeepasslib.Database
	kbdx string
	file *os.File
}

func NewKeypassInputConnector(kdbx string, password string) (*KeepassInputConnector, error) {
	file, err := os.Open(kdbx)
	if err != nil {
		return nil, err
	}

	db := gokeepasslib.NewDatabase()
	db.Credentials = gokeepasslib.NewPasswordCredentials(password)
	err = gokeepasslib.NewDecoder(file).Decode(db)
	if err != nil {
		return nil, err
	}

	err = db.UnlockProtectedEntries()
	if err != nil {
		return nil, err
	}

	abs, err := filepath.Abs(kdbx)
	if err != nil {
		return nil, err
	}

	return &KeepassInputConnector{
		db:   db,
		kbdx: abs,
		file: file,
	}, nil
}

func (ic KeepassInputConnector) GetName() string {
	return fmt.Sprintf("Keepass Database from '%s'", ic.kbdx)
}

func (ic KeepassInputConnector) ReadSecrets(args *lib.ReadSecretsArgs) (secrets []lib.ManagedSecret, err error) {
	var entries []gokeepasslib.Entry
	if args != nil {
		gn := &args.Group
		g := searchGroup(ic.db.Content.Root.Groups, *gn)
		if g == nil {
			return emptySecrets(), errors.New(fmt.Sprintf("cannot find group '%s'", *gn))
		}
		entries = g.Entries
		entries = append(entries, allEntries(g.Groups)...)
	} else {
		entries = allEntries(ic.db.Content.Root.Groups)
	}

	ss := make([]lib.ManagedSecret, len(entries))
	for i, e := range entries {
		ss[i] = lib.ManagedSecret{
			Name: e.GetTitle(),
		}

		if e.GetPassword() != "" {
			ss[i].Value = []byte(e.GetPassword())
		} else {
			if len(e.Binaries) == 0 {
				return emptySecrets(), errors.New(
					fmt.Sprintf("keepass entry '%s' has no password and no attachment", e.GetTitle()))
			}
			s, err := e.Binaries[0].Find(ic.db).GetContent()
			//s, err :=
			if err != nil {
				return emptySecrets(), err
			}
			ss[i].Value = []byte(s)
		}
	}

	return ss, nil
}

func (ic KeepassInputConnector) Close() error {
	return ic.file.Close()
}

func searchGroup(groups []gokeepasslib.Group, gn string) *gokeepasslib.Group {
	for _, g := range groups {
		if g.Name == gn {
			return &g
		}
		if len(g.Groups) != 0 && g.Name != IgnoredGroupName {
			return searchGroup(g.Groups, gn)
		}
		return nil
	}
	return nil
}

func allEntries(groups []gokeepasslib.Group) []gokeepasslib.Entry {
	entries := emptyEntries()
	for _, g := range groups {
		if g.Name != IgnoredGroupName {
			entries = append(entries, g.Entries...)
			if len(g.Groups) != 0 {
				entries = append(entries, allEntries(g.Groups)...)
			}
		}
	}
	return entries
}

func emptySecrets() []lib.ManagedSecret {
	return make([]lib.ManagedSecret, 0)
}

func emptyEntries() []gokeepasslib.Entry {
	return make([]gokeepasslib.Entry, 0)
}
