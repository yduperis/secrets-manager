package connector

import (
	"bytes"
	"fmt"
	"gitlab.com/yduperis/secrets-manager/lib"
	"io/ioutil"
	"strings"
	"testing"
)

const (
	kdbx     string = "../../tests/keepass_test.kdbx"
	testFile string = "../../tests/keepass_test_secret_file.txt"
	password string = "password"
	group    string = "group"
)

func TestKeepassInputConnector_GetName(t *testing.T) {
	ic, err := NewKeypassInputConnector(kdbx, password)
	if err != nil {
		t.Fatal(err)
	}
	defer ic.Close()

	n := ic.GetName()
	if !strings.HasPrefix(n, "Keepass Database from") {
		t.Error(fmt.Sprintf("Connector name must start with 'Keepass Database from', had '%s'", n))
	}

	if !strings.HasSuffix(n, "keepass_test.kdbx'") {
		t.Error(fmt.Sprintf("Connector name must points to the used KDBX file, had '%s'", n))
	}
}

func TestKeepassInputConnector_ReadSecrets(t *testing.T) {
	ic, err := NewKeypassInputConnector(kdbx, password)
	if err != nil {
		t.Fatal(err)
	}
	defer ic.Close()

	// TEST BASIC READ
	secrets, err := ic.ReadSecrets(nil)
	if err != nil {
		t.Fatal(err)
	}

	sn := make([]string, len(secrets))
	for i, s := range secrets {
		sn[i] = s.Name
	}

	if len(secrets) != 2 {
		t.Fatal(fmt.Sprintf("Expected 2 secrets read, had %d", len(secrets)))
	}

	if lib.SliceContains(sn, "ignored") {
		t.Fatal("Entry in .ignored groups must not be imported")
	}

	f, err := ioutil.ReadFile(testFile)
	if err != nil {
		t.Fatal(err)
	}

	res := bytes.Compare(secrets[0].Value, f)
	if res != 0 {
		t.Error(fmt.Sprintf("Invalid first secret value loaded, expected '%s' and had '%s'",
			string(f),
			string(secrets[0].Value),
		))
	}

	res = bytes.Compare(secrets[1].Value, []byte("totopassword"))
	if res != 0 {
		t.Error(fmt.Sprintf("Invalid second secret value loaded, expected 'totopassword' and had '%s'",
			string(secrets[1].Value)),
		)
	}

	// TEST GROUPS FILTERING
	secrets, err = ic.ReadSecrets(&lib.ReadSecretsArgs{Group: group})
	if err != nil {
		t.Fatal(err)
	}

	if len(secrets) != 1 {
		t.Fatal(fmt.Sprintf("Expected 1 secrets read, had %d", len(secrets)))
	}
	if secrets[0].Name != "test-password" {
		t.Error(fmt.Sprintf("Invalid secret read, expected 'test-password', had '%s'", secrets[0].Name))
	}
}
