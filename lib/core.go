package lib

func ImportSecrets(ic InputConnector, oc OutputConnector, readArgs *ReadSecretsArgs, writeArgs *WriteSecretsArgs) (ignoredExistingSecrets []ManagedSecret, err error) {
	if ic == nil{
		panic("Cannot import secrets from a nil InputConnector")
	}
	if oc == nil{
		panic("Cannot export secrets to a nil InputConnector")
	}

	secrets, err := (ic).ReadSecrets(readArgs)
	if err != nil {
		return make([]ManagedSecret, 0), err
	}

	ignoredExistingSecrets, err = (oc).WriteSecrets(secrets, writeArgs)
	if err != nil {
		return ignoredExistingSecrets, err
	}

	return ignoredExistingSecrets, nil
}
