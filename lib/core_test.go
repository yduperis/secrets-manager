package lib

import (
	"errors"
	"fmt"
	"testing"
)

var (
	mSecrets = []ManagedSecret{
		{
			Name:  "entry-1",
			Value: []byte("value-1"),
		},
		{
			Name:  "entry-2",
			Value: []byte("value-2"),
		},
		{
			Name:  "entry-3",
			Value: []byte("value-3"),
		},
	}
)

func TestImportSecrets(t *testing.T) {
	mic := NewMockIC()
	moc := NewMockOC()
	_, err := ImportSecrets(mic, moc, nil, nil)
	if err != nil {
		t.Fatal(err)
	}

	if !mic.ReadCalled {
		t.Fatal("InputConnector::ReadSecrets() not called")
	}

	if !moc.WriteCalled {
		t.Fatal("InputConnector::WriteSecrets() not called")
	}

	ss := moc.Buffer
	if len(ss) != 3 {
		t.Fatal(fmt.Sprintf("Invalid number of secrets exported, expected 3, had %d", len(ss)))
	}
	if ss[0].Name != "entry-1" {
		t.Error("Secrets order not preserved")
	}
	if ss[2].Name != "entry-3" {
		t.Error("Secrets order not preserved")
	}
	if string(ss[0].Value) != "value-1" {
		t.Error("Secrets value not preserved")
	}
	if string(ss[2].Value) != "value-3" {
		t.Error("Secrets value not preserved")
	}
}

type MockIC struct {
	ReadCalled bool
}

func (ic *MockIC) GetName() string {
	return "Mock Input Connector"
}

func (ic *MockIC) ReadSecrets(args *ReadSecretsArgs) ([]ManagedSecret, error) {
	if ic.ReadCalled {
		return make([]ManagedSecret, 0), errors.New("ReadSecrets() called multiple times !")
	}
	ic.ReadCalled = true
	return mSecrets, nil
}

func (ic *MockIC) Close() error {
	return nil
}

func NewMockIC() *MockIC {
	return &MockIC{ReadCalled: false}
}

type MockOC struct {
	Buffer      []ManagedSecret
	WriteCalled bool
}

func (oc *MockOC) GetName() string {
	return "Mock Output Connector"
}

func (oc *MockOC) WriteSecrets(secrets []ManagedSecret, writeArgs *WriteSecretsArgs) ([]ManagedSecret, error) {
	if oc.WriteCalled {
		return make([]ManagedSecret, 0), errors.New("WriteSecrets() called multiple times !")
	}
	oc.WriteCalled = true
	oc.Buffer = secrets
	return make([]ManagedSecret, 0), nil
}

func (oc *MockOC) Close() error {
	return nil
}

func NewMockOC() *MockOC {
	return &MockOC{Buffer: make([]ManagedSecret, 0), WriteCalled: false}
}
