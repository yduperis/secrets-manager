package lib

type InputConnector interface {
	ReadSecrets(args *ReadSecretsArgs) (secrets []ManagedSecret, err error)
	GetName() string
	Close() error
}

type OutputConnector interface {
	WriteSecrets(secrets []ManagedSecret, args *WriteSecretsArgs) ([]ManagedSecret, error)
	GetName() string
	Close() error
}

type ReadSecretsArgs struct {
	Group string
}

type ExistingSecretHandlingBehavior string

const (
	OverwriteExistingSecret ExistingSecretHandlingBehavior = "Overwrite"
	IgnoreExistingSecret    ExistingSecretHandlingBehavior = "Ignore"
	ErrorOnExistingSecret   ExistingSecretHandlingBehavior = "ErrorOnExistingSecret"
)

type WriteSecretsArgs struct {
	ExistingSecretHandlingBehavior
}
