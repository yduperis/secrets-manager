package lib

type ManagedSecret struct {
	Name  string
	Value []byte
}
