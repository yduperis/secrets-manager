package lib

func SliceContains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func MapContainsKey(m map[string]string, key string) bool {
	for k, _ := range m {
		if k == key {
			return true
		}
	}
	return false
}
