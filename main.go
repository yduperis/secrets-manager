package main

import (
	"github.com/urfave/cli/v2"
	"gitlab.com/yduperis/secrets-manager/cmd"
	"log"
	"os"
)

func main() {
	app := &cli.App{
		Name:      "secrets-manager",
		Usage:     "Manage secrets of a deployed project",
		UsageText: "secrets-manager - Manage secrets of a deployed project",
		Commands: []*cli.Command{
			cmd.CliVersionCmd(),
			cmd.CliExportCmd(),
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
